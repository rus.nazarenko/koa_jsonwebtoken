###### Application launch ######
To run the app, enter "docker-compose up --build"

###### You can use queries ######

POST http://localhost:3001/user/register
{
    "name": "Ruslan",
    "role": "admin",
    "email": "Ruslan@gmail.com",
    "password": "123"
}

POST http://localhost:3001/user/login
{
    "email": "Ruslan@gmail.com",
    "password": "123"
}

GET http://localhost:3001/user
(There must be a token in cookie or headers  )





###### You can use old queries ######



POST http://localhost:3001/user
{
    "user_name": "Vera",
    "role": "admin",
    "email": "Vera@gmail.com",
    "password": "123",
    "address_id": 1
}


DELETE http://localhost:3001/user/2

GET http://localhost:3001/user

PUT http://localhost:3001/user/1
{
    "user_name": "Kirill",
    "role": "super-admin",
    "email": "kirill@ukr.net",
    "password": "123456"
}




POST http://localhost:3001/address
{
    "country": "UK",
    "city": "London",
    "street": "Soborniy",
    "building": "783579"
}

GET http://localhost:3001/address
DELETE http://localhost:3001/address/2