import * as Koa from 'koa'
const app = new Koa()
import * as bodyParser from 'koa-bodyparser'
import userRouter from './src/components/users/userRouter'
import addressRouter from './src/components/address/addressRouter'
const db = require('./src/db/models')
const APP_PORT = process.env.APP_PORT

app.use(bodyParser())
app.use(userRouter.routes())
app.use(addressRouter.routes())

const some = async () => {
  await db.address.sync({ force: true });
  await db.user.sync({ force: true })
  app.listen(APP_PORT, () => {
    console.log(`server is listening on port ${APP_PORT}`)
  })
}

some();