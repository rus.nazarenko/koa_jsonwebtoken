'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {

    const data = [
      {
        street: 'Guardeyskiy',
        building: '147A',
        country: 'Ukraine',
        city: 'Kiev',
        created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      {
        street: 'prosto ulitsa',
        building: '14',
        country: 'UK',
        city: 'London',
        created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      {
        street: '565',
        building: '76',
        country: 'USA',
        city: 'NY',
        created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
      }
    ]
    await queryInterface.bulkInsert('addresses', data)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('addresses', {
      street: {
        [Op.in]: addresses.map((item) => item.street)
      },
      building: {
        [Op.in]: addresses.map((item) => item.building)
      },
      country: {
        [Op.in]: addresses.map((item) => item.country)
      },
      city: {
        [Op.in]: addresses.map((item) => item.city)
      }
    })
  }
};
