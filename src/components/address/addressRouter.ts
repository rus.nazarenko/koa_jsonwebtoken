import * as Router from 'koa-router'
import {
  addAddressController, delAddressController, updateAddressController, getAddressesController
} from './addressController'

const userRouter = new Router({ prefix: '/address' });


userRouter.post('/', addAddressController)
userRouter.del('/:id', delAddressController)
userRouter.put('/:id', updateAddressController)
userRouter.get('/', getAddressesController)

export default userRouter;