import { Context } from 'koa'
import { addAddressQuery, delAddressQuery, updateAddressQuery, getAddressesQuery } from './addressService'

export const addAddressController = async (ctx: Context) => {
  try {
    if (Object.entries(ctx.request.body).length === 0) {
      throw new Error("No data")
    }

    const data = {
      country: ctx.request.body.country,
      city: ctx.request.body.city,
      street: ctx.request.body.street,
      building: ctx.request.body.building
    }

    // const userId = ctx.request.body.userId

    const resAddUser = await addAddressQuery(data)
    ctx.status = 201
    ctx.body = { success: true, data: resAddUser }
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const delAddressController = async (ctx: Context) => {
  try {
    const data = [ctx.params.id]
    await delAddressQuery(data)
    ctx.status = 204
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const updateAddressController = async (ctx: Context) => {
  try {
    if (Object.entries(ctx.request.body).length === 0) {
      throw new Error("No data")
    }
    const data = {
      id: ctx.params.id,
      country: ctx.request.body.country,
      city: ctx.request.body.city,
      street: ctx.request.body.street,
      building: ctx.request.body.building
    }

    console.log("updateAddressController", data)

    await updateAddressQuery(data)
    ctx.status = 204
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const getAddressesController = async (ctx: Context) => {
  try {
    const resGetUsers = await getAddressesQuery()
    ctx.status = 200
    ctx.body = { success: true, data: resGetUsers }
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}