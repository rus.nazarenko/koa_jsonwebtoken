import * as Router from 'koa-router'
import {
  addUserController, delUserController, updateUserController,
  getUsersController, registerUserController, loginUserController, userVerificationController
} from './userController'

const userRouter = new Router({ prefix: '/user' });


userRouter.post('/register', registerUserController)
userRouter.post('/login', loginUserController)

userRouter.post('/', userVerificationController, addUserController)
userRouter.del('/:id', userVerificationController, delUserController)
userRouter.put('/:id', userVerificationController, updateUserController)
userRouter.get('/', userVerificationController, getUsersController)

export default userRouter;